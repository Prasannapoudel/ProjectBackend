import pool from "../db.js";
const createComplaint = (req, res, next) => {
  const id = req.user.id;

  const fileBuffer = req.file.buffer;

  const filesv = fileBuffer.toString("base64");
  console.log("entered");
  const { title, description, address, ward, category } = req.body;
  pool.query(
    "INSERT INTO complaint (user_id, title, description, address, ward,category,image) VALUES ($1, $2, $3, $4, $5, $6, $7)",
    [id, title, description, address, ward, category, filesv],
    (error, results) => {
      if (error) {
        next(error.message);
      } else {
        res.status(200).json({
          status: "success",
          message: "Complaint added sucessfully",
          // data: results.rows[0],
        });
      }
    }
  );
};
const getOwnComplaint = (req, res, next) => {
  const id = req.user.id;
  pool.query(
    "select * FROM complaint Where user_id = $1",
    [id],
    (error, results) => {
      if (error) {
        next("Failed to get");
      } else {
        res.status(200).json(results.rows);
      }
    }
  );
};

const getAllComplaint = (req, res, next) => {
  pool.query(
    "select  username,created_at, title, description, status, complaint.address, complaint.ward, category,priority, image from complaint inner join users on complaint.user_id = users.id ",
    (error, results) => {
      if (error) {
        next(error.message);
      } else {
        res.status(200).json(results.rows);
      }
    }
  );
};

const deleteComplaint = (req, res, next) => {
  const id = req.params.id;
  pool.query("DELETE FROM complaint WHERE id = $1", [id], (error, results) => {
    if (error) {
      next(error.message);
    } else {
      res.status(200).json({
        status: "success",
        message: "Complaint deleted sucessfully",
      });
    }
  });
};

const updatePriority = (req, res, next) => {
  const { id, name } = req.body;
  pool.query(
    "UPDATE complaint SET priority= $1 WHERE id=$2",
    [name, id],
    (error, results) => {
      if (error) {
        next(error);
      }
      res.status(200).json({ status: "success", message: "Priority updated" });
    }
  );
};

const updateStatus = (req, res, next) => {
  const { id, name } = req.body;
  pool.query(
    "UPDATE complaint SET status= $1 WHERE id=$2",
    [name, id],
    (error, results) => {
      if (error) {
        next(error);
      }
      res.status(200).json({ status: "success", message: "Status updated" });
    }
  );
};

const getComplaintStatus = (req, res, next) => {
  const id = req.user.id;
  pool.query(
    "SELECT status, COUNT(*) as count FROM complaint WHERE user_id = $1 AND status IN ('pending', 'hold', 'solved') GROUP BY status",
    [id],
    (error, results) => {
      if (error) {
        next(error.message);
      } else {
        const complaintCounts = {
          pendingComplaint: 0,
          holdComplaint: 0,
          solvedComplaint: 0,
        };

        // Process the results and update the counts object
        results.rows.forEach((row) => {
          if (row.status === "pending") {
            complaintCounts.pendingComplaint = parseInt(row.count);
          } else if (row.status === "hold") {
            complaintCounts.holdComplaint = parseInt(row.count);
          } else if (row.status === "solved") {
            complaintCounts.solvedComplaint = parseInt(row.count);
          }
        });

        res.status(200).json(complaintCounts);
      }
    }
  );
};

export {
  createComplaint,
  deleteComplaint,
  getAllComplaint,
  getComplaintStatus,
  getOwnComplaint,
  updatePriority,
  updateStatus,
};
